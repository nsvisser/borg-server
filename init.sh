#!/bin/bash
if [ -d "/keys" ]
then
	echo "Setting up ssh keys..."
	for key in /keys/*; do
		echo "Adding $(basename $key)..."
		cp $key /etc/ssh/$(basename $key)
		if [[ "$key" == *.pub ]]
		then
			echo "Permissions 644 for /etc/ssh/$(basename $key)"
			chmod 644 /etc/ssh/$(basename $key)
		else
			echo "Permissions 600 for /etc/ssh/$(basename $key)"
			chmod 600 /etc/ssh/$(basename $key)
		fi
	done
else
	echo "Cannot find keys, skipping key installation"
fi
service ssh start
tail -f /dev/null