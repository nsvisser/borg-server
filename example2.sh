#!/bin/bash
export BORG_PASSPHRASE="..."
export BORG_SRH="ssh -i ~/.ssh/id_rsa -p 10022"
export BACKUP_NAME=`date +%Y%m%d_%H%M%S`
echo "Backup name: $BACKUP_NAME"
borg create ssh://borg@127.0.0.1:10022/var/backups/test1::projects-${BACKUP_NAME} /mnt/c/files/projects