FROM ubuntu:22.10
RUN apt-get update && \
	apt-get install -y borgbackup net-tools openssh-server
RUN useradd -ms /bin/bash borg
ADD id_rsa.pub /home/borg/.ssh/authorized_keys
RUN chown borg:borg -R /home/borg/.ssh
RUN chmod 700 -R /home/borg/.ssh
LABEL maintainer="nsvisser"
LABEL version="0.1"
LABEL description="Borg backup server"
ADD init.sh /init.sh
ENTRYPOINT ["/init.sh"]