#!/bin/bash
docker stop borg-server
docker rm borg-server
# -v /mnt/c/files/projects/prive/borg-server/keys:/keys
docker run -d \
	-v /mnt/c/files/backups:/backups \
	-v /mnt/c/files/projects/prive/borg-server/keys:/keys \
	--name borg-server \
	-p 0.0.0.0:10022:22/tcp \
	borg-server:v0.1